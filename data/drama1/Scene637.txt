지금 내 마음이 어떤지 알아맞춰봐.
내맘하고 똑같을걸?
자기도 그래? 같이 있기 지겨운데 빨랑 집에 가서 자야지. 그생각했어?
야아. 
 이렇게 보고 있어도 또 보고 싶다.
나두.
사랑에 빠진 사람들은 다들 그런다길래 그게 무슨 말인가 했어. 이제 알거같애.
나두.
빨리 결혼해서 같이 있고 싶다 하루종일.
나두.
따라쟁이. 
우리 참 신기해. 어떻게 그렇게 만났지? 삼겹살집에서 그 난리를 칠때, 자기가 단속 나와서 처음봤지. 난 취해서 기억은 못하지만.
그날 지구대 근무 나간게 지금 생각해도 신기해. 아무래도 우릴 만나게 할려고 운명이 시켰나봐.
난 가끔 그런 생각해. 이렇게 좋은 사람을 좀더 좋은 장소, 좋은 때에 만났으면 더 좋았을걸...보여주고 싶지 않은 내 과거 다 보여준것도 챙피하고, 나이트클럽에서 오다가다 부킹한 것도 걸리고. 남녀사인 언제 어디서 만났는가도 중요한데. 그치?
아니. 난 그런건 괜찮은데, 좀더 빨리 만났으면 좋았겠다 그생각은 해본적 있어.
난 지금이 더 좋아. 어려서 만났으면 우린 쉽게 헤어졌을지도 몰라. 내성질에 자기한테 못되게 굴었을거고, 티격태격하다가 소중한줄도 모르고 쉽게 헤어졌을지도 모르지. 우리 웃긴다. 첨만난 얘기 아무리해도 안 질려. 늘 신기하고.
나두.
뽀뽀하고 싶다.
나두.
 따라쟁이. 이상이는 어영이 따라쟁이래요.  해줘.
