 엄청난, 너 자꾸 엄청난 짓할래?
 내가 뭐?
현찰이한테 이십 뜯어내고 셋째 제수씨한테 이십 뜯어냈대며.
뜯어냈다는 말은 심하다. 어머니가 백만원 만들어내래서 그렇게 했어 왜? 그게 엄청난 짓이야?
중간에 띵겨 먹은게 엄청난 짓이란 말이지.
안띵겨 먹었어. 물어봐. 딱 이십씩만 받았다니까. 내가 띵겨 먹었대?
우리도 똑같이 이십 냈어야지. 그거 몇푼이나 한다고. 돈 몇푼에 형제간 의 갈라지면 되겠냐?
양쪽에 이십씩 걷는대신 난 다리품 팔고 입 아프고, 쌤쌤 비겨도 돼.
잔통박 굴리지 마라. 너처럼 머리 굴리다가 일번 토낀다 이번 쌩깐다 삼번 띵겨먹는다 이런애 나오면 어쩔래? 맘보 곱게 써야 이쁜 애가 나오지.
온갖 심부름 다 하면서 그정도는 뗄수 있지. 막말로 수수료 뗐다 생각해. 은행에서도 수수료 떼잖아.
프라임 고객은 수수료도 없다. 내가 그래도 장남인데 만원이라도 더 내야지. 앞으로 무조건 똑같이 걷고 내가 만원 더 낸다. 다시 말해, 현찰이네 33, 이상이네 33, 우리 34. 오케이? 알아 들었냐?
 만원이 뉘집 개 이름이야? 만원 벌려면 쌔가 빠지는데. 장남이라고 물려받은 재산이 있어, 보태준게 있어? 만원이면 라면이 몇갠데.
스으.
