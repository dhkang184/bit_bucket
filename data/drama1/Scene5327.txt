아이구, 여보. 우리가 자식이 아니라 웬수를 키웠습디다. 세상에 이런 날벼락이 어딨대요? 내가 가슴이 덜덜 떨리고 입이 안떨어져서 말이 안나오네. 어떻게 이런 일이 있어요? 큰애 처녀 아니에요. 숨겨논 자식까지 있었대요. 그 자식 키우겠다고 떠억하니 끌고 들오는데, 내가 기가 막히고 코가 막히고,
...
당신, 알고 있었어요? 알고 있었냐구요?
그렇게 됐대.
 첨부터 알고 있었어요?
첨부터는 아니고..
그럼 언제요? 언제 알았어요?
언제 알았냐가 뭐가 중요해. 진정해. 화부터 내지말고 차근차근,
 내가 지금 화 안내게 생겼어요? 그거 알고도 가만있었어요? 우리 건강이가 어디가 부족해서 저런 막돼먹은 여시하고 살아요?
그럼 이제와서 어쩌라고. 건강이가 좋다면 할수없는거지.
뭐에요?  미친놈. 아주 지 인생 망칠려고 작정을 했어. 자식까지 딸린여자 어디가 좋다고 집구석까지 끌고 들와? 당장 나가, 당장! 내눈에 흙이 들어가기 전까지 어림짝도 없다. 딴꼴 다봐도 그꼴만은 못봐!
엄마 미안해. 진작 말할려고 했는데 엄마 놀랠까봐 말 못했어. 그냥 받아들여줘.
절대 못받아줘. 사람을 놀려도 유분수지, 어디서 낯짝 두껍게 처녀행세를 해?  에라이, 못난놈. 어떻게 애딸린것도 모르고 결혼까지 해? 그 쌩쑈에 넘어간 우리가 그렇지. 내 이걸 당장. 
 엄마.
비켜. 어디 내집에 발을 들여?
우선 열부터 내려. 이래갖고 무슨 해결을 봐?
해결 보긴 뭘 봐요? 무조건 쫓아내야죠. 당장 내보내라. 안그랬다간 너죽고 나죽을줄 알아.
엄마, 한번만. 내가 좋아서 한 결혼이야. 나도 힘들게 결정했어. 내가 괜찮다는데, 내가 내자식으로 키우면 될거 아냐.
뭐? 이게 아주 돌다 돌다 정신줄까지 놔버렸네. 저 여시같은게 어떻게 꼬셨길래 니 자식으로 키우겠대? 남의 자식 키우기가 쉬운줄 알어? 에라이 못난놈. 
그만해! 일단 나가 있어라.
