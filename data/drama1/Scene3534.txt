 당신 왜 가만있어요? 시에미가 말하는데 씨알도 안먹히는데 저걸 그냥 놔둬야 돼요?
씨알이 먹히게 해야 먹히지. 조회는 아무나 서? 조회설 생각하지말고 도나 닦아. 도 닦으면 수명도 길어진대. 도 많이 닦아서 애들 많이 뚜들겨 잡고 오래오래 살어.
이양반이? 자식들이 이렇게 속?이는데 오래 살고 싶어도 못살겠네요.
툭하면 며느리들 잡는데 그러지말어.
잡게 생기면 잡아야죠.
그래서, 며느리 잡아서 살림살이 좀 나아졌어? 당신 앞에서는 예 어머니, 하고 돌아가면 결국 누구 잡겠어?
개라도 잡는데요?
개가 아니라 지 남편들 잡지. 지 남편들이 누구야? 바로 당신이 낳은 귀한 아들들이야. 왜 그 생각을 못해? 한번만 생각하면 답이 나오는걸. 나는 뭐 첨부터 며느리들이 이뻐서 오냐오냐 하는줄 알어? 생판 첨본 며느리 뭐가 이쁘겠어? 내자식하고 사니까 이쁜거지. 어머니한테 당한거 까먹었어? 어머니한테 당할때마다 나한테 고대로 해댔잖아. 그렇게 해보고도 몰라? 나 그때 배웠어. 며느리한테 잘해야 내아들 성하다는거. 우리 아들들 지마누라들한테 볶여서 빼짝빼짝 말르는거 보기싫으면 그만해. 당신은 도닦고 오래오래 사는데 아들들은 제명에 못살고 말라 죽어가면 좋아?
 누가 그렇대요? 맨날 나만 틀렸대지 나만.
넘치지 말어. 미친거로 보여.
