 그게 어떻게 모은 돈인데..못먹고 못입고..허리띠 쫄라매면서도 그돈만은 안건들려고 이악물었는데. 이럴줄 알았으면 맛있는거나 실컷 먹고 좋은옷이나 실컷 입고 좋은데나 실컷 구경다닐걸. 그렇게 좋아하는 고기도 맘대로 못먹고 입고싶은 옷도 못입고 제주도 여행 한번 못갔는데...
늙어서 자식들한테 손 안벌릴려고, 당신 퇴직금 안건들려고 얼마나 알뜰살뜰한줄 알아요? 미리 땡겨 쓸것도 안쓰고 보따리 장사까지 하면서 참았는데, 언놈 좋으라고 그입에다 홀랑 털어줘요?  이럴려고 그 고생했나. 당신 만나서 사십년 고생한게 겨우 이거에요?  이러구 앉았지 말고 빨리 가서 찾아와요! 당장 가서 찾아오라구요!
