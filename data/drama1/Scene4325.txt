저녁 먹을까?
천천히 먹지뭐. 차부터 한잔해.
많이 말랐다.
일땜에 신경써서 그런가봐. 자기도 말랐네.
어쩌다 우리가 이렇게 됐는지.
지금 우리 이모습...부부같지가 않아. 너무 멀리 온거 같애. 맨처음 우리 두사람 사이에 틈이 보였을때 얼른 그 틈을 메꿀려고 서로가 노력했으면 여기까지 오진 않았을텐데.. 이미 너무 늦어버린거 같애.
늦지 않았어. 나도 떨어져 있는동안 많이 생각해봤어. 우리한테 애기라도 있었다면 적어도 이렇게 멀어지진 않았겠지 그런 생각이 들드라.
 또 애기 얘기구나. 난 딴거 다 빼고 우리둘만 갖고 얘기했으면 좋겠어. 우리 두사람한테 아직 사랑이 남아있는지, 다시 좋은 관계가 될 여지가 있는지 그런거. 애기땜에 이렇게 된건 아니잖아. 애기갖는 문제는 이미 합의봤고 지금 우리가 이렇게 된건,
잠깐, 애기 얘길 하자는게 아니라, 만약에 우리한테 애기가 있었다면 이런식으로 멀어지진 않았을거고, 싸울일이 있어도 좀더 참지 않았을까 그런 생각이 든다 이말이었어. 어쩌면 나도 좀더 자길 배려했을지도 모르고.
자긴 어쩜 이렇게 똑같니?
뭐가.
애기만 있었으면 우리가 이러진 않았을거라는거. 난 그말이 썩 기분좋진 않아. 물론 애기가 있었으면 달랐겠지. 자기도 나도. 난 애기 때문이 아니라 순전히 우리 두사람이 얼마나 깊이 사랑하나, 아직도 사랑이 남아있나, 이런걸로 우리문제를 해결하고 싶어. 오늘 자기가 만나자고해서 설레는 맘으로 달려 나왔어. 요 근래 느껴보지 못한 설레임으로 가슴 두근거리면서 이 자리에 나왔다고. 근데 결국 애기얘기나 하고. 난 뭐니? 난 너한테 아무것도 아니니? 결국 우리 문제는 애기가 없는게 문제였다로 몰아갈려고?
그런거 아냐.
입장 바꿔 생각해봐. 우리 며칠만에 만난줄 알아? 우리 부부야. 멀리 떨어진것도 아니고 5분밖에 안떨어진 거리에서 별거아닌 별거하다 오늘 처음 만났어. 근데... 무슨 중대한 회의나 할려고 만난 사람들처럼...남편이란 사람이 이렇게 어색하고 낯설어 보이는거...이런 느낌 나만 그런거니?
너도 그렇겠지. 이젠 우리한테 아무 감정도 남아있지 않은거 같애. 부영이말 듣고 괜시 이런자리 만든거 같은데 진심으로 우러나기 전엔 이런거 하지마. 아무래도 우린 아직 대화할 준비가 안된거 같애. 이렇게 변한 우리가 너무 맘이 아프다. 
