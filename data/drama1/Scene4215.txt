 나 못가. 우리집 차례 준비해야돼.
뭐?
우리집 딸만 있는 집이야. 내가 장남노릇 하는 집이라고. 아부지 혼자 놔두고 어떻게 가? 자기집은 나 없어도 되지만 우리집은 나 없으면 안돼.
왜, 그런 생각 안해봤어? 아들없는집 사위로 들왔으면 적어도 그런 생각쯤은 했을줄 알았어.
일하는 아줌마 계시잖아.
명절엔 일하는 사람도 쉬게 해야지. 그리고 이건 도우미가 하는거하곤 달라.
그래도 첫 명절인데 시댁에 가야되는거 아냐?
자긴 이런 상황 보고도 그런 말이 나와? 안그래도 형님 여러번 전화 오셨는데 내가 바깥일 땜에 바쁘다고 했어. 일일이 설명하기 싫어서. 나하나 없는셈 치면 되잖아. 그동안 나없이도 잘만 지내놓고 꼭 내가 있어야돼? 그리고 진짜로 홈쇼핑방송이라도 있었으면 어쩔뻔했어? 그럼 일도 때려치고 가야돼?
일있는건 아니잖아. 엄마랑 형수님이 어떻게 생각하겠어? 난 자기가 한소리 들을까봐 그래.
자기가 막아줘. 내마누라 여차저차 사정있어 못간다 그렇게. 왜 못해?
너무 어려운거 시키지말고,
뭐가 어려워?
어려워. 작전 나가 칼든 놈하고 싸우는거보다 어려워. 그냥, 얼른 가서 얼굴만 비치고 오자.
자기 너무한다. 가서 얼굴만 비치고 온다는게 말이돼? 여태 말했잖아. 나도 우리집 차례 지내야될 막중한 임무를 타고난 사람이라고. 왜 내가 우리집 차례 걷어치고 자기네집 가야돼? 자기네 집에 사람이 없는것도 아닌데 이건 너무 부당하잖아.
그런거 따지지말고, 그냥, 내말에 따라주면 안되겠어? 우리집에선 용납안돼. 그러니 설득시킬려면 시간이 필요한걸 당장 해내라고 하면 나더러 어떡하라고? 집에 가서 싸워? 마누라 사정 이러니 명절 참석못하겠다 선언하고 칠푼이 소리 듣고 부모형제 끊고 살면 좋겠어?
비약하지마. 꼭 그런식으로 막나가는 사람처럼 말하냐? 자기한테 내가 너무 큰 기대를 했나봐. 그냥 내편 들어주기 바랬는데.  안그래도 속상한데 이런거까지 신경쓸려니까 미치겠어 진짜.
 왜 짜증은 내? 장인어른 들으시게.
 안그래도 속상해 죽겠단 말야. 아까부터 전화는 계속오지, 속은 타지, 나도 죽겠어 나도. 난 뭐 안가는게 편해서 이러구 있는줄 알아? 차라리 가서 일하는게 백번 편해. 나도 어쩔수 없는 며느리니까. 근데 내 사정이 이런걸 왜 눈치보면서 내편 들어달라고 사정까지 해야되냐고?
울지 말고 말해. 울일은 아니잖아.
 안울게 해줘. 안그래도 부영이땜에 죽겠는데.
처제가 왜?
무슨 일이야? 처제한테 무슨일 있어? 말해봐.
