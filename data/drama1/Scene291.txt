동서.
 형님, 웬일이세요?
지나가는 길에 들렀어. 자,  나 맨손으로 안왔다?
고마워요 형님, 이쪽으로 앉으세요.
 사무실 좋다. 샵도 돈냄새 확 나던데 거기있는거 다 진짜야?
진짜도 있고 모조품도 있어요.
그거 다 털면 떼부자 되겠다.
예?
농담. 맨날 집에서 볼땐 되게 후져 보였는데 사무실에서 보니까 좀..
괜찮아 보여요?
그건 모르겠는데, 있어 보이긴 한다.
 차 뭘로 드릴까요?
차는 됐구, 대신 부탁하나만 들어주라.
부탁요?
참 인생이란게 그렇드라. 내뜻대로 안되고 가진게 없으니 내맘대로도 안되고, 내가 이런소리 한다고 되도 않은 소리 한다고 할지 몰라도 인생 혼자 사는거 아니잖아. 거지가 밥얻어 먹으러 와도 밥한숟갈 주는거, 누가 아쉬운 소리 하면 쫓아내지 않고 들어주는거, 이게 다 죽어서 좋은데 갈라고 착한일 하는거잖아. 미리 차표 사놓는셈치고. 안그래?
무슨 말씀이신지?...
내말 먼말인지 몰라?
예.
동서 대학 나온거 맞어? 돈 꿔달라고.
예?
말귀 못알아먹어? 왜자꾸 예?예? 그래? 내가 여기 뭐 얻어먹으러 왔겠어? 형제 좋다는게 뭐야? 급하게 돈필요할때 서로 꿔주는게 형제지. 삼천만 땡겨주라.  아니, 사천만 땡겨달라고 해야되나?
 형님. 저한테 그만한 돈도 없지만요, 있다해도 형님하고 돈거래 하고 싶지 않아요.
어머, 웃긴다. 그럼 나하고 돈거래 안하면 누구하고 할건데?
아무리 가까워도, 아무리 부모형제라도 돈거래 안하는걸로 배웠어요.
어머, 가까운 부모형제끼리 돈거래 안하면 누구랑해? 지나가는 사람 붙잡고 돈꿔달라해? 사람들 되게 웃겨. 툭하면 가까운 사이에 돈거래 하는거 아니라는데, 그럼 먼데 사람하고 돈거래하나? 그냥 꿔주기 싫으면 싫다고 하지 꼭 그렇게 기분나쁘게 말하드라.
저는요, 돈 안꿔주는 사람보다 돈꿔달라고 말하는 사람이 더 나쁘다고 생각해요. 이건 꿔줘도 불편하고 안꿔줘도 불편하고, 전 형님하고 돈거래 하고 싶지 않아요. 지금은 매정하게 들리실지 몰라도 할수없어요. 이후로도 돈꿔달란 소린 안하셨으면 좋겠어요.
