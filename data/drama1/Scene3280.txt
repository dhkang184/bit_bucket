 어떻게 손아래 동서가 감히 형님한테 눈똑바로 뜨고 덤빌수가 있어? 어머니도 나한테 꼼짝못하고 쩔쩔매시는데. 눈치코치도 없이 저래갖고 어떻게 인생을 살어?
제수씨 걱정말고 니 걱정이나 해.
오빠, 난 딴건 몰라도 살림은 때려죽인데도 못해.
못하면 배워야지. 넌 이집 큰며느리야. 난 제수씨만 보면 미안해서 얼굴을 못들겠다. 둘째며느리로 시집와서 십년동안 부모님 모시고 맏며느리 역할 다했어. 내 뒤치닥거리까지 하고. 니가 나이는 어려도 형님이야. 윗사람 노릇하는게 쉬운줄 아냐? 형님이면 형님답게 행동해.
형님대접을 해야,
스으.
