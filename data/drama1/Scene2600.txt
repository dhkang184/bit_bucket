신났다 신났어. 그럼 낼부터 학생이냐? 어떻게 공부할 생각을 다했어?
난 누구한테 개무시 당하곤 못살잖아.
누가 널 개무시 했는데? 오빠가?
아니. 있어. 은혜를 웬수로 갚는 놈. 지가 나 아니면 어디서 태어났다고 감히 무시해? 더이상 묻지마.
안물어도 알겠다. 니가 딱 종남이 수준이다. 그래도 넌 늦복이 터졌나부다. 그집에서 공부하라고 허락도 해주고.
응. 난 복이 터졌는데, 언닌 왜 복이 오다말어? 구대삼촌한테 연락 없었어?
연락하겠니? 만정 떨어졌겠지.
떨어질 정이나 있었나? 웃긴다. 뭐 그딴 사람이 다 있어? 남자가 여자 과거갖고 어쩌고저쩌고, 쪼잔하게. 울오빠 봐. 나같은 여자도 그냥 데리고 살잖아.
내가 잘못했지. 잔뜩 내숭떨다가 나 발랑 까진 여자에요 했으니 얼마나 놀랐겠냐.
 생각나지?
됐어. 그나마 깊은정 안들어서 다행이다.
많이 좋아했는데. 보고싶지?
말시키지마. 오늘 이거 다 닦아야돼. 
