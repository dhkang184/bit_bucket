김이상 경감입니다.
앉으세요.
 어제 작전은 잘 됐나요?
예?
그런 어설픈 여장으로도 먹혔나봐요? 누가봐도 한편의 코메디던데.
보셨습니까? 극비로 진행한건데 어떻게 아시고..
검사 허투로 보지 마세요. 나름대로 정보망이 있어요. 그런 어설픈 여장을 하고도 일망타진해서 다행이지만, 중요한건 앞으로에요. 확실한 증거가 없으면 이리저리 다 빠져나가니 철저히 대비하세요. 그리고 인권문제 중요한거 아시죠? 각별히 신경쓰시구요. 가보세요.
그것땜에 부르셨습니까?
예.  왜요?
아닙니다.
겨우 그런 얘기라면 전화로 해도 될텐데 바쁜 사람 굳이 검찰청까지 들오게 하고, 은근히 시집살이 시키는거 아냐? 맞죠?
예.
 솔직하시네요. 전 디지털보다 아날로그 방식을 좋아해서요. 뭐든 얼굴보고 하는게 낫지않겠어요? 우리가 적이 아니라는걸 확인도 할겸.
