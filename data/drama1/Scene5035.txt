마약중독자들 체포하는것보다 들여오는 마약을 차단하는게 급선무에요. 다들 이번 작전에 긴장들 하시구요. 고생들 하셨어요.
고생하셨습니다.
오늘은 일단 쉬시고 낼부터 스케쥴표 나온대로 바로 강훈련 들어갈겁니다.
병아리가 독수리 흉내내다 날개 부러집니다.
괜찮으세요?
 이 정도갖고. 일부러 그런겁니다.
나이 들어봐. 한번씩 움직일때마다 온몸의 뼈들이 우두둑 뿌두둑 난리들이다. 조경장, 가방이 왜 이렇게 두툼해? 총각이 뭘 그렇게 많이 싸왔어?
저요? 
 어디 문딸일 있냐? 도둑질 하러 가는것도 아니고.
무슨일 벌어질지 모르니 만반의 준비를 해왔죠.
그러는 유경위님은 뭘 싸오셨는데요? 유경위님 가방도 만만찮습니다.
 이정도는 되야지. 
이게 다 뭡니까?
만에 하나 여장할수도 있잖아. 나 가끔 그날 생각난다. 그날 이후로 내 미모가 만만찮다는거 뼈저리게 느꼈어. 그래서 뼈소리 나잖냐. 
그 버릇 또 나왔습니까? 한동안 끊더니.
큰일만 맡고 긴장하면 꼭 이래. 옛날에 딱 끊었는데 또 시작이다. 이거 심각한대.
웃을일 아닙니다. 전 심각하다고요.
최경위님은요? 그 가방도 만만찮은데.
저야 당연히 최우선적으로 만반의 준비를 해왔습니다. 사람이 끝까지 살아야되지 않겠습니까?
이건 뭐에 쓸려고 싸오셨습니까?
작전중 무인도에 고립될 경우를 대비한겁니다. 낚싯대로 고기를 잡고 매운탕 끓이고 라면사리 넣서 끓이면 기가 막힙니다.
오늘따라 최경위님하고 영 안어울립니다.
그러는 백경위님은 뭘 가져오셨는데요?
아무리 생선 고기 잡으면 뭐합니까? 양념이 있어야지.  여차하면 무기로도 쓸수 있습니다. 
 만화 그만봐. 
팀장님은요?
난 심플하게  권총, 수갑.
역시 기본에 충실하라는 말이 이럴때 나오는 말이야. 팀장님 존경합니다. 다 집어넣 집어넣. 
자 그럼 낼새벽 4시 기상입니다. 늦지않게 준비하십시오.
 그래도 라면은 끓여먹고 자야 되는거 아닌가요?
 와후.
역시 검사님 멋재이.
