 쯧쯧...못난놈, 오죽 못났으면 마누라가 바람을 펴?
말만 들어도 살이 떨려 죽겠네요. 어떻게 막돼먹어도 그렇게 막돼 먹었는지. 그때 어떡하든 결혼 못하게 뜯어말렸어야 했는데. 멀쩡한 남의 자식 흠집 냈잖아요.
내자식이 틀려먹었지 남의 자식 탓할거 없어.
그게 왜 내자식 탓이에요? 바람핀년이 나쁜년이지. 그래도 큰애가 속이 깊어요. 지속이 썩어 문드러졌을텐데 혼자 다 견디고 이혼할때도 얼마나 고민이 많았겠어요? 남들같으면 식구들 떼거리로 몰려가서 해대고 개망신을 줬을텐데, 그걸 혼자 어떻게 견뎠대? 챙피해서 엇다가 말도 못했을거고.
....
그러니 당신도 너무 뭐라고 하지 말아요. 지속은 오죽하겠어요?
그것도 다 지팔자지. 못난놈. 
