 우리 둘이 볼일 없을텐데요.
다신 우리 종남이 앞에 나타나지 마십시요.
 내새끼 내가 본다는데 당신이 먼상관이야?
엄밀히 말하면, 그쪽 새끼가 아니라 청난이 새끼죠. 청난이 혼자 낳서 기르는동안 교도소 들락거리느라 책임도 못져놓고 이제와서 무슨 아버집니까?
시끄럽고, 용건 끝났으면 일어나지. 
분명 경고 했습니다? 다시한번 종남이앞에 알짱대면, 가만 안있습니다.
 그럼 이렇게 하지. 종남이한테 물어보자고.
뭐요?
종남이 불러다 놓고, 내가 니 진짜 아빤데, 가짜아빠랑 살고 싶냐, 진짜 아빠랑 살고 싶냐? 직접 선택하라면 되겠네.
 니놈이 애비냐? 너같은놈 애비될 자격없어. 니 생각만 하지말고 애생각도 해! 그 어린게 받을 충격은 생각안해? 양쪽에 떡을 쥐고, 어느것을 먹을까요 알아맞춰봅시다, 그거 하냐? 너 이런놈인거 보니까 내맘이 더 확실해졌다. 종남이 절대 너같은놈한테 안보내. 종남이한테 손끝하나 댔다간 각오해. 
