 옷입어. 가게.
안간다고 몇번 말해요? 보쌈 먹고 싶어 환장한것도 아니고.
그래도 자식이 장사 시작해서 개업한다는데 부모가 돼갖고 들여다 봐야지.
부모 싫다고 나간 자식, 뭐하러 들여다 봐요? 난 안가요. 지들이 먼저 고개 숙이고 들오면 몰라도. 내가 미쳤어요?
안그래도 미친거루 보여. 우리 잘한거 없어. 장사좀 하겠다고 돈해달라는거 못해줬잖아.
돈 못해준 부모는 다 죄인이래요? 말되는 소릴 해야지. 나 델꼬 갈 생각말고 당신이나 가요.
