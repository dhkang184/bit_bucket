큰형. 여기.
 바쁜 사람 뭐하러 불러내냐? 너도 바쁘면서.
아무리 바빠도 밥은 먹어야지. 작은 형한테 허락 받았어. 태실장한테도 허락 받았고.
지롤.
냄새 죽이지? 한우 꽃등심인데 마블링 봐. 죽여주지. 먹어봐.
됐다. 맥주나 한잔 주라.
일단 고기부터 먹어. 이러다 형 쓰러지면 어떡해. 안그래도 날아가게 생겼구만.
형수한테 연락은 해봤어? 어딨는지는 알아?
