나 바쁘고 피곤해. 용건있으면 빨랑해. 단 일분도 너랑 마주 앉아 있고 싶지 않다.
사람이 왜그러니? 좀 여유있게 살지않고. 아무리 내가 밉다고 대놓고 직설적으로 유치하게. 좀 돌려 말하면 안되니?
됐고.
너야말로 됐고, 너네 이혼한다며?
 그걸 니가 어떻게 알어?
우연히 들었어. 저번에 김사장 배달가다 만났어. 서류 제출하고 숙려기간이라며. 뭘 숙려하는지 모르겠지만.
 내 예감이 맞았네. 니들 그런거지? 신경 안쓰니까 떳떳하게 말해. 뒤에서 호박씨 까지말고.
아직도 미련 잔뜩 남아 보인다? 이혼한다면서 아직도 감정 남아있니?
너네 둘한테 속은게 분하고 억울해서 그래. 왜, 우리 이혼하는데 관심있니?
관심보다도 미안해서. 나땜에 이렇게 된거 같애서 그냥 모른척 넘어가긴 좀 그렇드라.
진작에 이혼했어야 되는데 여기까지 끌고왔다. 그 생각만 해도 분해.
미안해.
이제와서 미안해? 그말 할려고 나 불렀니? 미친. 
잠깐만.  김사장이 앞으로 어떤 결정을 하든, 말하자면 이혼한 다음에 나랑 어떤 결정을 하든 정말로 상관없니? 
 그 생각까진 안해봤는데, 이혼까지 한 마당에 재혼을 하든 삼혼을 하든 누구랑 붙어살든 알깨나 뭐니? 그럴거 같으면 이혼 뭐하러 해? 그건 왜 물어보는데?
그냥 니맘이 궁금해서.
나 만사 귀찮은 사람이야. 새끼들 델꾸 사는것만도 벅차. 내가 살림이나 했지 언제 장사같은거 해봤니? 하루종일 손님들 대하고 다리 아프게 종종거리고 밤이면 팔다리 허리아파서 잠도 못자. 이 고생하는거 다 그인간 때문이다 생각하면 이가 빡빡갈려. 남편이 아니라 웬수다 웬수. 진작에 이혼 못한게 한스러워.
솔직히 말해봐. 그때 이인간이 너한테 먼저 찝쩍댔지?
응?
내 남편이니까 무조건 너 미워하고 너한테 뭐라고 했는데, 여자가 먼저 그러기 쉽지 않지. 더구나 넌 내 친군데 니가 먼저 그랬겠니? 그 인간이 먼저 꼬리쳤겠지.
뭐..이제와서 그런 말하면 뭐하니.
그때 이혼하자고 방방떴더니 죽어도 이혼은 못한대드라. 그래서 너한테 쫓아가서 내 존심 세워주면 이혼안하겠다 했더니 그건 죽어도 못한다드라? 그때 알아봤다.
 못한대?
그래. 마누라가 쑈라도 한번 해달라 악을 쓰는데도 못하겠대. 너한테 상처 못준다고. 그때 생각하면, 
 그러더니 부모님한테 다 말하겠다니, 그제야 겁나는지 나 끌고가서 쌩쑈해주드라. 알잖아. 지부모라면 벌벌 떠는거.
알지.
그때 알았어. 니가 미친게 아니라 그인간이 먼저 미쳐 날뛰었다는거.
 그꼬라지 보고도 애들땜에 할수없이 살았다. 그렇게 믿음이 한번 깨지고 나니까 만정 떨어져서 더는 못살겠드라. 지잘못으로 전재산까지 날리고 나 쌩고생 시키는거 생각하면 꼴도 보기 싫고 목소리도 듣기 싫어. 이혼확정판결 날때까지 나가 있으라고 해도 약점 잡힐까봐 싫댄다. 인간이 어떻게 뻔뻔한지. 마침 배달할 사람도 없어서 부려먹고 있어. 어찌됐든 난 위자료하고 양육비는 넉넉히 받을거야. 그래야 나도 새끼랑 먹고 살거 아니니?
애들은 니가 키우게?
당연하지. 말하다 보니 또 열받네. 
