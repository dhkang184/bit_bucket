 죽여라 죽여. 팰라면 패고 죽일려면 죽이고 니맘대로 해. 나 오늘 마지막 유서까지 쓰고 나왔다. 하지만 종남인 영원히 내 아들이야. 내 호적에 오른 김종남이라고. 내가 오늘 여기서 니 손에 죽더라도 그 사실은 변함없다. 그건 알고 있어라.
 종남이 잘키워. 니 아들 김종남 잘 키우라고.
나같은놈 종남이아빠 될 자격없어. 교도소나 들락대는 놈이 무슨 아빠야?  그놈의 자식 낳는것도 못봤고 한번 안아주지도 못했어. 데리고 놀러 간적도 없고. 내 자식 세상에 내놓고도 아빠 노릇 못한 죄책감을 니가 알어? 그니까 당신이 잘키워. 내 몫까지. 안그랬다간 어떻게 되는지 알지? 항상 지켜볼거야. 
 야! 그냥가면 어떡하냐. 맞짱 떴으면 뒷풀이는 하고 가야지.
아무 걱정말고 하행선씨 인생 사십시오. 아직 저보다 한참 아래인거 같은데 좋은 사람 만나서 새가정도 이루고요. 종남이는 최선 다해서 잘 키우겠습니다. 종남이 이제 내아들입니다. 이담에 만났을때 절대 부끄럽지 않은 사람으로 키우겠습니다.
 제가 웬만하면 물러서지 않았을겁니다. 근데 쭉 지켜보니까 솔직히 저같은 놈보다 종남이한테 훨씬 좋아아빠 되줄거 같아서 포기한겁니다.  차라리 당신이 나쁜놈이었음 좋겠습니다. 잉잉.
뭐 썩 좋은 놈은 아닙니다.  어디로 갈겁니까?
서울 떠날겁니다. 춘천쪽에서 일자리가 들왔거든요.
급한거 아니면 하루만 시간좀 내주세요.
