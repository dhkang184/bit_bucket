 병원에 갔다왔어?
미쳤어? 병원에 가게?
엄마맘도 안좋지?
...
결혼하래.
아부지 병원 계시는거 보고 느낀게 많나봐. 엄마한테도 새엄마 대접할거래.
...
엄마맘 땡기는대로 해. 가고 싶으면 가.
됐어. 한창 뜨거서 몸부림칠땐 못가게 말리드만 인제 다 식어빠지니께 가라고? 나 꿈깼다. 이 나이에 무슨 시집. 김서방 보기도 그렇고 손주새끼들 보기도 그렇고.
대가리 떼고 꼬랑지 떼고, 딱 말해. 갈거야, 말거야?
말껴.
그럼 오늘 이후로 다신 말 안한다. 그쪽 입장 딱하게 됐다고 엄마등 떠미는거 아냐. 엄마도 사장님 좋아하잖아. 엄만 정리 다했다고 하지만 내가 볼땐 맘정리 개코나 못했어. 동서도 저정도면 백기 든거나 한가지고. 엄마만 좋다면 난 찬성이야. 김서방이고 손주새끼들한테 민망한거야 눈한번 딱 감으면 되는거고, 엄마 원하는대로 해.
기양 장사나 하고 돈이나 벌란다.
내숭떨지 말고. 엄마 얼굴에 다 써있어.
참말여. 내가 이 나이에 뭐드러 내숭떨겄냐. 이러구 사는게 편혀. 다 늙어서 늙은 영감탱이 밥수발하고 병수발 들일 있가니? 기양 나 혼자 편하게 살란다.
선 안봤대.
여자 얼굴도 안보고 그냥 나가버리드래. 그럼 알만하잖아.
